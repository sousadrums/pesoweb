
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no"/>
	<title>Pesos Laura y Jorge</title>
	<link rel="icon" type="image/x-icon" href="<?=base_url?>assets/img/logo.png" />
	<link rel="stylesheet" type="text/css" href="<?=base_url?>assets/css/styles.css" />
</head>
<body>
	<div id="container">
		<!--Cabecera-->
		<header id="header">
			<div id="logo">
				<img src="<?=base_url?>assets/img/pesospeq.png" alt="logo"/>
				<a href="<?=base_url?>"><h1 id="logo">Pesos Laura y Jorge</h1></a>
			</div>
		</header>
		<!--Menu-->
		<div id="menus">
			<?php if(!isset($_SESSION['identity'])): ?>
				<nav id="menu2">
					<ul>
						<li><a href="<?=base_url?>usuario/loginV">Inicio sesión</a></li>
						<!--<li><a href="<?=base_url?>usuario/register">regístrate</a></li>-->
					</ul>
				</nav>
			<?php else: ?>
				<nav id="menu2">
					<ul>
						<li><a href="<?=base_url?>usuario/loged"><?=$_SESSION['identity']->Nombre?> <?=$_SESSION['identity']->Apellidos?> </a></li>
						<?php if (!isset($_SESSION['nutri'])): ?>
							<li><a href="<?=base_url?>peso/introducir">Introducir peso</a></li>
						<?php endif; ?>
						<li><a href="<?=base_url?>usuario/logout">Cerrar sesión</a></li>
					</ul>
				</nav>
			<?php endif; ?>
		</div><br>
<div id="cursos">
		<h2>Usuarios</h2>
	<br/>
	<table>
		<tr>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Acciones</th>
		</tr>
		<?php while ($usr=$usuarios->fetch_object()) : ?>
			<tr>
				<td><?=$usr->Nombre; ?></td>
				<td><?=$usr->Apellidos; ?></td>
				<td><a href="<?=base_url?>peso/ver&id=<?=$usr->id?>" class="button">Registros</a>
					<?php if (isset($_SESSION['admin'])) : ?>
						<a href="<?=base_url?>usuario/delete&id=<?=$usr->id?>" class="button button-danger">Eliminar</a>
					<?php endif; ?>

				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>
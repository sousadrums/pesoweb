<?php

class Usuario{
	private $id;
	private $nombre;
	private $apellidos;
	private $email;
	private $password;
	private $nacimiento;
	private $talla;
	private $rol;
	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}

	function getId(){
		return $this->id;
	}
	function getNombre(){
		return $this->nombre;
	}
	function getApellidos(){
		return $this->apellidos;
	}
	function getEmail(){
		return $this->email;
	}
	function getPassword(){
		return  password_hash($this->db->real_escape_string($this->password), PASSWORD_BCRYPT, ['cost'=> 4]);
	}
	function getNacimiento(){
		return $this->nacimiento;
	}
	function getTalla(){
		return $this->talla;
	}
	function getRol(){
		return $this->rol;
	}
	

	function setId($id){
		$this->id=$id;
	}
	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}
	function setApellidos($apellidos){
		$this->apellidos=$this->db->real_escape_string($apellidos);
	}
	function setEmail($email){
		$this->email=$this->db->real_escape_string($email);
	}
	function setPassword($password){
		$this->password= $password;
	}
	function setNacimiento($nacimiento){
		$this->nacimiento=$nacimiento;
	}
	function setTalla($talla){
		$this->talla=$talla;
	}
	function setRol($rol){
		$this->rol=$rol;
	}
	


	public function save(){
		$sql = "INSERT INTO usuarios VALUES(NULL, '{$this->getNombre()}', '{$this->getApellidos()}', '{$this->getEmail()}', '{$this->getPassword()}', '{$this->getNacimiento()}', '{$this->getTalla()}', 'user')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function login(){
		$result = false;
		$email = $this->email;
		$password = $this->password;
		
		// Comprobar si existe el usuario
		$sql = "SELECT * FROM usuarios WHERE correo = '$email'";
		$login = $this->db->query($sql);
		
		
		if($login && $login->num_rows == 1){
			$usuario = $login->fetch_object();
			
			// Verificar la contraseña
			$verify = password_verify($password, $usuario->password);
			if($verify){
				$result = $usuario;
			}
		}
		
		return $result;
	}

	public function edit(){
		$sql="UPDATE usuarios SET Nombre='{$this->getNombre()}', Apellidos='{$this->getApellidos()}', correo='{$this->getEmail()}', nacimiento='{$this->getNacimiento()}', talla='{$this->getTalla()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function updatePassword(){
		$sql="UPDATE usuarios SET password='{$this->getPassword()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
	
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;

	}

	public function getAll(){
		$sql="SELECT * FROM usuarios WHERE rol='user' ORDER BY id DESC";
		$usuarios= $this->db->query($sql);
		return $usuarios;

	}

	public function delete(){
		$sql="DELETE FROM usuarios WHERE id={$this->id}";
		$delete1=$this->db->query($sql);
		$sql2="DELETE FROM peso WHERE usuario_id={$this->id}";
		$delete2=$this->db->query($sql2);
		$result=false;
		if ($delete1 && $delete2) {
			$result=true;
		}
		return $result;
	}

}



?>